<?php
$reg = 0;
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();
include 'presentacion/menuAdministrador.php';
if (isset($_GET['usu'])) {
    $reg = 1;
}
else if(isset($_GET['obr'])){
    $reg = 2;
}else if(isset($_GET['jurado'])){
    $reg = 3;
}else if(isset($_GET['tutor'])){
    $reg = 4;
}
?>
<div class="container" style="margin-top: 150px;">
	<div class="row">
		<div class="col-12">
			<div class="card">
			<?php if($reg == 1){?>
				<div class="alert alert-success" role="alert">
				Usuario -- <?php echo $_GET['nombre'] . " " . $_GET['apellido']?> -- registrado
				</div>
			<?php }else if($reg == 2){?>
				<div class="alert alert-success" role="alert">
				Obrero -- <?php echo $_GET['nombre'] . " " . $_GET['apellido']?> -- registrado
				</div>
			<?php }else if($reg == 3){?>
				<div class="alert alert-success" role="alert">
					Jurado -- <?php echo $_GET['nombre'] . " " . $_GET['apellido']?> -- registrado
				</div>
			
			<?php }else if($reg == 4){?>
				<div class="alert alert-success" role="alert">
					Tutor -- <?php echo $_GET['nombre'] . " " . $_GET['apellido']?> -- registrado
				</div>
			<?php }?>
				<div class="card-header bg-primary text-white">Bienvenido Administrador</div>
				<div class="card-body">
					<p>Administrador: <?php echo $administrador -> getNombre() . " " . $administrador -> getApellido() ?></p>
					<p>Correo: <?php echo $administrador -> getCorreo(); ?></p>
					<p>Hoy es: <?php echo date("d-M-Y"); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>
