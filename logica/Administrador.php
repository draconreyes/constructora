<?php

class Administrador extends Persona {
    private $administradorDAO;
    private $conexion;
    private $foto;
    private $telefono;
    /**
     * @return AdministradorDAO
     */
    public function getAdministradorDAO()
    {
        return $this->administradorDAO;
    }

    /**
     * @return Conexion
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    /**
     * @return  <string, mixed>
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @return  <string, mixed>
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param AdministradorDAO $administradorDAO
     */
    public function setAdministradorDAO($administradorDAO)
    {
        $this->administradorDAO = $administradorDAO;
    }

    /**
     * @param Conexion $conexion
     */
    public function setConexion($conexion)
    {
        $this->conexion = $conexion;
    }

    /**
     * @param Ambigous <string, mixed> $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }

    /**
     * @param Ambigous <string, mixed> $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    function Administrador ($id="", $nombre="", $apellido="", $correo="", $clave="",$telefono="",$foto=""){
        $this -> Persona($id, $nombre, $apellido, $correo, $clave);
        $this->conexion = new Conexion();
        $this->foto=$foto;
        $this->telefono=$telefono;
        $this->administradorDAO = new AdministradorDAO($id, $nombre, $apellido, $correo, $clave,$telefono="",$foto="");
    }
    
    function autenticar() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->administradorDAO->autenticar());
        if($this->conexion->numFilas()==1){
            $resultado = $this->conexion->extraer();
            $this->id = $resultado[0];
            $this->conexion->cerrar();
            return true;
        }
        else {
            $this->conexion->cerrar();
            return false;
        }
    }
    
    function consultar() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->administradorDAO->consultar());
        $resultado = $this->conexion->extraer();
        $this->id=$resultado[0];
        $this->nombre=$resultado[1];
        $this->apellido=$resultado[2];
        $this->correo=$resultado[3];
        $this->telefono=$resultado[4];
        $this->foto=$resultado[5];
        $this->conexion->cerrar();
    }
    
    function consultarProyCurr() {
    	$this->conexion->abrir();
    	$this->conexion->ejecutar($this->administradorDAO->consultarProyCurr());
    	$proyectos = array();
    	$i=0;
    	while(($resultados = $this -> conexion -> extraer()) != null){
    		$proyectos[$i] = array($resultados[0], $resultados[1]);
    		$i++;
    	}  
    	$this->conexion->cerrar();
    	return $proyectos;
    	
    }
}
?>