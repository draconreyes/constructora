<?php
class ObreroDAO extends Persona{
    private $foto;
    private $telefono;
    /**
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }
    
    /**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }
    
    /**
     * @param string $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }
    
    /**
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }
    
    
    function ObreroDAO ($id="", $nombre="", $apellido="", $correo="", $clave="",$telefono="",$foto=""){
        $this -> Persona($id, $nombre, $apellido, $correo, $clave);
        $this->foto=$foto;
        $this->telefono=$telefono;
        
    }
    
    function autenticar() {
        return "SELECT idobrero
                FROM obrero
                WHERE correo = '" . $this->correo . "' AND
                clave = md5('" . $this->clave . "');";
        
    }
    
    function consultar(){
        return "select idobrero, nombre, apellido, correo,telefono,foto from obrero
                where idobrero = '" . $this -> id . "'";
    }
    function existeCorreo() {
        return "SELECT idobrero
					FROM obrero
					WHERE correo = '" . $this->correo ."';";
    }
    function registrar() {
        return "INSERT INTO obrero (nombre, apellido, correo, clave,telefono,foto)
        VALUES ('".$this->nombre."','".$this->apellido."','".$this->correo."',md5('".$this->clave."'),'".$this->telefono."','".$this->foto."');";
    }
    function actualizarFoto()
    {
        return "UPDATE obrero
                SET foto = '" . $this->foto . "'
                WHERE idobrero = " . $this->id;
    }
    function fotoExiste()
    {
        return "SELECT foto
                FROM obrero
                WHERE idobrero = " . $this->id;
    }
}

?>